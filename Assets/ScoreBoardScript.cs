﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//To add a specific amount
//ScoreBoardScript.AddScore(50);
//To add 10 (the normal)
//ScoreBoardScript.AddScore();
//Should be usable from any script
public class ScoreBoardScript : MonoBehaviour
{
    private static int _score = 0;

    private void OnEnable()
    {
        ProgressBar.onFull += AddScore;
    }

    private void OnDisable()
    {
        ProgressBar.onFull -= AddScore;
    }

    private static int score 
    {
        get 
        {
            return _score;
        }
        set 
        {
            _score = value;
            UpdateText();
        }
    }
    private static Text scoreText;

    // Start is called before the first frame update
    void Start()
    {
        scoreText = GetComponent<Text>();
        score = 0;
    }

    // Update is called once per frame
    static void UpdateText()
    {
        scoreText.text = "SCORE: " + score;
    }

    public static void AddScore(int amount = 10) 
    {
        score += amount;
    }

    public static void AddScore()
    {
        AddScore(100);
        SoundManagerScript.PlaySound("PowerUp");
    }
}
