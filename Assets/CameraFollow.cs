﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public GameObject follow;
    float ypos;

    private void Start()
    {
        ypos = transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = follow.transform.position;
        pos.z = -10;
        pos.y = ypos;

        transform.position = pos;
    }
}
