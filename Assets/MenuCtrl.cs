﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuCtrl : MonoBehaviour
{

    void Start() 
    { 
        MusicManagerScript.PlaySound("TitleTheme");
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.anyKey)
        {
            SceneManager.LoadScene("SampleScene", LoadSceneMode.Single);
        }
    }
}
