﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Versioning;
using UnityEngine;

public class SoundManagerScript : MonoBehaviour
{
    public static AudioClip EndChimeSound, JumpSound, PowerUpSound, SlashSound;
    static AudioSource audioSrc;

    //Access thing for sounds : SoundManagerScript.PlaySound("");

    // Start is called before the first frame update
    void Start()
    {
        EndChimeSound = Resources.Load<AudioClip>("EndChime");
        JumpSound = Resources.Load<AudioClip>("Jump");
        PowerUpSound = Resources.Load<AudioClip>("PowerUp");
        SlashSound = Resources.Load<AudioClip>("Slash");

        audioSrc = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static void PlaySound (string clip) 
    {
        switch (clip) 
        {
            case "EndChime":
                audioSrc.PlayOneShot(EndChimeSound);
                break;
            case "Jump":
                audioSrc.PlayOneShot(JumpSound);
                break;
            case "PowerUp":
                audioSrc.PlayOneShot(PowerUpSound);
                break;
            case "Slash":
                audioSrc.PlayOneShot(SlashSound);
                break;
        }
    }
}



