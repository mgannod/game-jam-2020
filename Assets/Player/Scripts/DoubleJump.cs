﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoubleJump : Skill
{
    [SerializeField] private float rotationSpeed = 50;

    Vector3 oneFrameAgo;

    private bool canDJump = false;
    private bool rotate = false;

    private Rigidbody2D playerbody;

    [SerializeField] private float jumpForce = 10f;

    void Start()
    {
        playerbody = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if (Input.GetButtonDown("Jump") && canDJump)
        {
            DoDoubleJump();
        }
    }

    private void FixedUpdate()
    {
        if (rotate)
        {
            Vector3 velocity = transform.position - oneFrameAgo;
            oneFrameAgo = transform.position;
            float direction = velocity.x > 0 ? -1 : 1;
            playerbody.transform.rotation *= Quaternion.Euler(0, 0, rotationSpeed * direction);
        }
    }

    public bool GetRotate()
    {
        return rotate;
    }


    public void DoDoubleJump()
    {
        playerbody.velocity = new Vector2(0, 0);
        playerbody.AddForce(new Vector3(0, jumpForce), ForceMode2D.Impulse);
        SoundManagerScript.PlaySound("Jump");
        canDJump = false;
        rotate = true;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.collider.tag == "Platform")
        {
            canDJump = false;
            rotate = false;

            transform.rotation = Quaternion.Euler(0, 0, 0);
        }

        if (collision.collider.tag == "Wall")
        {
            rotate = false;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider.tag == "Platform")
        {
            canDJump = true;
        }
    }
}