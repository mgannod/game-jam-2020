﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackRadius : MonoBehaviour
{

    private void OnTriggerStay2D(Collider2D collision)
    {
        bool rotate = GetComponentInParent<DoubleJump>().GetRotate();
        Debug.Log(rotate);
        if (rotate && collision.tag == "Enemy")
        {
            collision.GetComponent<Debt>().KillDebt();
        }

        if(collision.tag == "Credit")
        {
            collision.GetComponent<Credit>().CatchCredit();
        }
    }
}
