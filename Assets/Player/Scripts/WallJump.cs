﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallJump : Skill
{
    private Rigidbody2D playerbody;

    private bool onWall = false;
    private bool falling;
    private bool wallFalling;
    private bool wallJumping;
    private bool firstTouch = true;
    [SerializeField] private float xJumpForce = 15;
    [SerializeField] private float yJumpForce = 30;
    [SerializeField] private float wallJumpTime = 0.05f;

    private float GRAVITYSCALE;

    private void OnEnable()
    {
        Debug.Log("Added Wall Jump");
    }

    // Start is called before the first frame update
    void Start()
    {
        playerbody = GetComponent<Rigidbody2D>();
        GRAVITYSCALE = playerbody.gravityScale;
    }

    #region update 
    // Update is called once per frame
    void Update()
    {
        HandleWallFalling();
        HandleWallJump();
    }

    public void HandleWallFalling()
    {
        falling = playerbody.velocity.y < 0;

        wallFalling = onWall && falling;
        if (wallFalling)
        {
            if (firstTouch)
            {
                playerbody.velocity = new Vector2(0, -.1f);
                firstTouch = false;
            }
            playerbody.gravityScale = GRAVITYSCALE * .1f;
        }
        else
        {
            playerbody.gravityScale = GRAVITYSCALE;
            firstTouch = true;
        }
    }

    public void HandleWallJump()
    {
        float movementDirection = Input.GetAxis("Horizontal");
        if (Input.GetButtonDown("Jump") && onWall)
        {
            wallJumping = true;
            Invoke("WallJumpFalse", wallJumpTime);
        }

        if(wallJumping)
        {
            playerbody.velocity = new Vector2(xJumpForce * -movementDirection, yJumpForce);
        }
    }
    #endregion

    private void WallJumpFalse()
    {
        wallJumping = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Wall")
        {
            onWall = true;

            transform.rotation = Quaternion.Euler(0, 0, 0);
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider.tag == "Wall")
        {
            onWall = false;
        }
    }
}