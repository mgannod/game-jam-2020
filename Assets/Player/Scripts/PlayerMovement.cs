﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D), typeof(BoxCollider2D))]
public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private Rigidbody2D playerbody;
    private SpriteRenderer renderer;
    
    private float horizontalMove = 0f;
    private bool jump;
    [SerializeField] private float moveSpeed = 5f;
    [SerializeField] private float jumpForce = 10f;

    private Animator animator;
   
    // Start is called before the first frame update
    void Start()
    {
        playerbody = GetComponent<Rigidbody2D>();
        renderer = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        horizontalMove = Input.GetAxis("Horizontal");
        if (Input.GetButton("Jump") && Mathf.Abs(playerbody.velocity.y) < 0.001f)
        {
            jump = true;
        }
    }

    public float getJumpForce()
    {
        return jumpForce;
    }

    private void FixedUpdate()
    {
        HandleMovement();
    }

    private void HandleMovement()
    {
        Vector3 movementVector = new Vector3(horizontalMove, 0, 0);
        transform.position += new Vector3(horizontalMove, 0, 0) * moveSpeed * Time.deltaTime;

        if(!Mathf.Approximately(0, horizontalMove))
        {
            renderer.flipX = (horizontalMove < 0);
            if(!jump)
                animator.SetBool("isWalking", true);
        }
        else
        {
            if(!jump)
                animator.SetBool("isWalking", false);
        }

        if(jump)
        {
            playerbody.AddForce(new Vector3(0, jumpForce), ForceMode2D.Impulse);
            animator.SetTrigger("jumpTrigger");
            SoundManagerScript.PlaySound("Jump");
            jump = false;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Platform")
        {
            animator.SetTrigger("landed");
        }
    }
}
