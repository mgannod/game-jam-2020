﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class SkillHandler : MonoBehaviour
{
    [SerializeField] private Skill[] availableSkills;

    private void OnEnable()
    {
        ProgressBar.onFull += AddRandomSkill;
    }

    private void OnDisable()
    {
        ProgressBar.onFull -= AddRandomSkill;
    }

    // Start is called before the first frame update
    void Start()
    {
        for(int i = 0; i < availableSkills.Length; i++)
        {
            availableSkills[i].enabled = false;
        }
    }

    public void AddRandomSkill()
    {
        List<Skill> temp = new List<Skill>(availableSkills);
        temp = temp.Where(x => x.enabled == false).ToList();
        Debug.Log(temp);
        RandomObject(temp).enabled = true;
    }

    public static T RandomObject<T>(List<T> list)
    {
        int r = Random.Range(0, list.Count);
        if (list != null && list.Count() > 0)
            return list[r];
        return default(T);
    }
}