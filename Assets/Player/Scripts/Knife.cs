﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Knife : MonoBehaviour
{
    public GameObject target;
    public GameObject head;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    void Update()
    {
        bool flip = head.GetComponent<SpriteRenderer>().flipX;
        GetComponent<SpriteRenderer>().flipX = flip;
        
        int direction = flip ? -1 : 1;

        if (GetComponentInParent<DoubleJump>().GetRotate())
        {
            // Spin the object around the target at 100 degrees/second.
            transform.RotateAround(target.transform.position, Vector3.back, 200 * Time.deltaTime * direction);
        }
        else
            transform.RotateAround(target.transform.position, Vector3.back, 200 * Time.deltaTime * direction);
    }
}
