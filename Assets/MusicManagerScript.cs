﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Versioning;
using UnityEngine;

public class MusicManagerScript : MonoBehaviour
{
    static private bool BattleThemeBool;
    static private bool even;
    public static AudioClip BattleIntroSound, BattleLoop1Sound, BattleLoop2Sound, TitleThemeSound;
    static AudioSource audioSrc;

    //Access thing for sounds : MusicManagerScript.PlaySound("");

    // Start is called before the first frame update
    void Start()
    {
        BattleIntroSound = Resources.Load<AudioClip>("BattleIntro");
        BattleLoop1Sound = Resources.Load<AudioClip>("BattleLoop1");
        BattleLoop2Sound = Resources.Load<AudioClip>("BattleLoop2");
        TitleThemeSound = Resources.Load<AudioClip>("TitleTheme");

        audioSrc = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (BattleThemeBool) 
        {
            if (!audioSrc.isPlaying) {
                if (even) { PlaySound("BattleLoop1"); even = false; }
                if (!even) { PlaySound("BattleLoop2"); even = true; }
            }
        }
    }

    public static void PlaySound(string clip)
    {
        switch (clip)
        {
            case "BattleIntro":
                audioSrc.PlayOneShot(BattleIntroSound);
                break;
            case "BattleLoop1":
                audioSrc.PlayOneShot(BattleLoop1Sound);
                break;
            case "BattleLoop2":
                audioSrc.PlayOneShot(BattleLoop2Sound);
                break;
            case "TitleTheme":
                audioSrc.PlayOneShot(TitleThemeSound);
                break;
        }
    }

    public static void PlayBattleTheme() 
    {
        BattleThemeBool = true;
        even = false;
        PlaySound("BattleIntro");
    }
}



