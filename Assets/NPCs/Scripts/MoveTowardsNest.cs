﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Threading;
using UnityEngine;

public class MoveTowardsNest : MonoBehaviour
{
    public Nest target;
    public float speed = 5;
    private Vector3 offscreenTarget;
    private bool activated = false;
    public int abductSpeed = 2;

    SpriteRenderer debtRenderer;
    Debt debt;

    void Start() 
    {
        target = NestManager.GetTarget();
        debtRenderer = GetComponent<SpriteRenderer>();
        debt = GetComponent<Debt>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!activated)
        {
            if (target == null) return;
            this.transform.position = Vector3.MoveTowards(this.transform.position, target.transform.position, Time.deltaTime * speed);
            if (Vector3.Distance(this.transform.position, target.transform.position) < .001f)
            {
                activated = true;
                offscreenTarget = this.transform.position + new Vector3(0, 50, 0);
                target.AbductBird(debt);
            }
        }
        else
        {
            if (activated)
            {
                this.transform.position = Vector3.MoveTowards(this.transform.position, offscreenTarget, Time.deltaTime * abductSpeed);
                if (Vector3.Distance(this.transform.position, offscreenTarget) < 1)
                {
                    debt.KillBaby();
                }
            }
        }
    
        Vector2 dire = GetComponent<Rigidbody2D>().velocity;
        debtRenderer.flipX = (dire.x < 0);
    }
}
