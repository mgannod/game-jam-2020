﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditManager : MonoBehaviour
{
    [SerializeField] private GameObject creditPrefab;

    float timer = 0;
    [SerializeField] private float maxTime;
    private Vector2 camOffset;
    [SerializeField] private int spawnDistance;

    private void Start()
    {
        maxTime = Random.Range(maxTime, maxTime + 5);
        camOffset = Camera.main.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if(timer >= maxTime)
        {
            timer = 0;
            SpawnCredit();
        }
    }

    private void SpawnCredit()
    {
        GameObject temp = Instantiate(creditPrefab);
        float x = Random.Range(-1f, 1f);
        temp.GetComponent<Credit>().SetDirection(x > 0 ? 1 : -1);
        float y = Random.Range(-1f, 1f);
        Vector3 spawn = new Vector3(x, y, 0);
        spawn = spawn.normalized * spawnDistance;
        spawn += (Vector3)camOffset;
        temp.transform.position = spawn;
        maxTime = Random.Range(maxTime, maxTime + 5);
    }
}
