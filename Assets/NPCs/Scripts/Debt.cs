﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Debt : MonoBehaviour
{
    private BabyBird bird;

    public bool dead = false;

    private void Update()
    {
        if (dead == true)
            KillDebt();
    }

    public void KillDebt()
    {
        if(bird != null)
            bird.Release();
        ScoreBoardScript.AddScore(10);
        SoundManagerScript.PlaySound("Slash");
        Destroy(gameObject);
    }

    public void SetBaby(BabyBird bird)
    {
        this.bird = bird;
    }

    public void KillBaby()
    {
        Destroy(bird.gameObject);
        Destroy(gameObject);
    }


}
