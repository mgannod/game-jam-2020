﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;

public class DebtManager : MonoBehaviour
{
    public int spawnDistance;
    public GameObject debtPrefab;
    public float spawnDelay;
    private float countDown;
    private Vector2 camOffset;

    // Start is called before the first frame update
    void Start()
    {
        countDown = Random.Range(spawnDelay, spawnDelay + 1);
        camOffset = Camera.main.transform.position;
        MusicManagerScript.PlayBattleTheme();
    }

    // Update is called once per frame
    void Update()
    {
        countDown -= Time.deltaTime;
        if (countDown < 0) { SpawnDebt();}
    }

    private void SpawnDebt() 
    {
        GameObject temp = Instantiate(debtPrefab);
        float x = Random.Range(-1f, 1f);
        float y = Random.Range(-1f, 1f);
        Vector3 spawn = new Vector3(x, y, 0);
        spawn = spawn.normalized * spawnDistance;
        spawn += (Vector3)camOffset;
        temp.transform.position = spawn;
        //temp.GetComponent<MoveTowardsNest>().target = NestManager.GetTarget();
        countDown = Random.Range(spawnDelay, spawnDelay + 1);
        spawnDelay -= 0.05f;
    } 
}
