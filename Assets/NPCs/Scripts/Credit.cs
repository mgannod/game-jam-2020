﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Credit : MonoBehaviour
{
    int direction = 1;
    [SerializeField] private float speed;

    // Update is called once per frame
    void Update()
    {
        Vector3 target = new Vector3(direction * 50, 0, transform.position.z);
        this.transform.position = Vector3.MoveTowards(this.transform.position, target, Time.deltaTime * speed);
    }

    public void SetDirection(int direction)
    {
        this.direction = -1 * direction;
    }

    public void CatchCredit()
    {
        ScoreBoardScript.AddScore(10);
        Destroy(gameObject);
    }
}
