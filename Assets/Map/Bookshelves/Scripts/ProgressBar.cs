﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class ProgressBar : MonoBehaviour
{
    private Image progressBar;
    private Bookshelf bookshelf;
    private bool cooldown = false;

    private float maxValue;

    public delegate void OnFullDelegate();

    public static OnFullDelegate onFull;

    private float cooldownTime;
    private float cooldownTimeMax;

    private void Start()
    {
        bookshelf = GetComponent<Bookshelf>();
        progressBar = new List<Image>(GetComponentsInChildren<Image>()).Where(x => x.gameObject.name == "ProgressBar").ToList()[0];
        if (bookshelf == null)
            Debug.LogError("Bookself Broke");
        if (progressBar == null)
            Debug.LogError("Progress Bar Broke");
        maxValue = bookshelf.GetMaxValue();
        progressBar.fillAmount = 0;
        cooldownTimeMax = 2 * bookshelf.GetMaxValue();
    }

    void Update()
    {
        if(!cooldown)
        {
            progressBar.fillAmount = GetFillAmount(bookshelf.GetCounter(), maxValue);

            if (progressBar.fillAmount == 1)
            {
                cooldown = true;
                Debug.Log("Full Bar");
                onFull?.Invoke();
            }
        }

        if(cooldown)
        {
            cooldownTime = Mathf.Clamp(cooldownTime + Time.deltaTime, 0, cooldownTimeMax);
            progressBar.fillAmount = 1 - GetFillAmount(cooldownTime, cooldownTimeMax);

            if(progressBar.fillAmount == 0)
            {
                cooldownTime = 0;
                cooldown = false;
                bookshelf.ResetCounter();
            }
        }
    }

    private float GetFillAmount(float min, float max) => min / max;
}
