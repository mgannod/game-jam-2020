﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class Bookshelf : MonoBehaviour
{
    [SerializeField] private SkillHandler player;
    private bool reading = false;

    [SerializeField] private float maxValue = 10;
    [SerializeField] private float counter;

    // Start is called before the first frame update
    void Start()
    {
        counter = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if(reading && counter < maxValue)
        {
            counter = Mathf.Clamp(counter + Time.deltaTime, 0, maxValue);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Player")
        {
            reading = true;
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            reading = false;
        }
    }

    public float GetCounter()
    {
        return counter;
    }

    public float GetMaxValue()
    {
        return maxValue;
    }

    public void ResetCounter()
    {
        counter = 0;
    }
}
