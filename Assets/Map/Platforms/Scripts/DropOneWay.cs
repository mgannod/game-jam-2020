﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlatformEffector2D), typeof(BoxCollider2D), typeof(Rigidbody2D))]
public class DropOneWay : MonoBehaviour
{
    private PlatformEffector2D effector;
    private float waitTime;
    private const float MAXWAIT = 0.05f;

    private bool touchingPlayer = false;
    [SerializeField] private Rigidbody2D playerbody;
    // Start is called before the first frame update
    void Start()
    {
        effector = GetComponent<PlatformEffector2D>();
        playerbody = GameObject.FindWithTag("Player").GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetAxis("Vertical") == 0)
        {
            waitTime = MAXWAIT;
        }

        bool falling = playerbody.velocity.y <= 0.001f;
        bool holdingDown = Input.GetAxis("Vertical") == -1;

        if(falling && holdingDown)
        {
            if (waitTime <= 0)
            {
                effector.rotationalOffset = 180f;
                waitTime = MAXWAIT;
            }
            else
            {
                waitTime -= Time.deltaTime;
            }
        }
        else
        {
            effector.rotationalOffset = 0;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.transform.tag == "Player")
        {
            touchingPlayer = true;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider.transform.tag == "Player")
        {
            touchingPlayer = false;
        }
    }
}
