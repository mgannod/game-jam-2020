﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlatformEffector2D), typeof(BoxCollider2D), typeof(Rigidbody2D))]
public class MovingPlatform : MonoBehaviour
{
    [SerializeField] private Transform target;
    private Vector3 startPos;
    private Vector3 targetPos;

    [SerializeField] private float moveSpeed;

    private bool towardsTarget = true;

    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.position;
        targetPos = target.transform.position;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        HandleMovement();

    }

    private void HandleMovement()
    {
        float step = moveSpeed * Time.deltaTime;

        if (towardsTarget)
            transform.position = Vector3.MoveTowards(transform.position, targetPos, step);
        else
            transform.position = Vector3.MoveTowards(transform.position, startPos, step);

        if(Vector3.Distance(transform.position, targetPos) < 0.001)
        {
            towardsTarget = false;
        }
        else if(Vector3.Distance(transform.position, startPos) < 0.001)
        {
            towardsTarget = true;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        collision.collider.transform.SetParent(transform);
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        collision.collider.transform.SetParent(null);
    }
}
