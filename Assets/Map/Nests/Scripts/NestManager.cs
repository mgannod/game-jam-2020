﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using UnityEngine;

public class NestManager : MonoBehaviour
{
    public GameObject nestPrefab;
    public GameObject[] spawnPoints;
    public int nestCount = 1;
    private static List<Nest> nestList;

    // Start is called before the first frame update

    void Start()
    {
        nestList = new List<Nest>();
        if (nestCount > spawnPoints.Length){nestCount = spawnPoints.Length;}
        List<GameObject> tempSpawns = new List<GameObject>(spawnPoints);
        tempSpawns = Shuffle(tempSpawns);
        for (int i = 0; i < nestCount; i++) 
        {
            GameObject temp = Instantiate(nestPrefab);
            temp.transform.position = tempSpawns[0].transform.position;
            tempSpawns.RemoveAt(0);
            nestList.Add(temp.GetComponent<Nest>());
        }    
    }

    public static Nest GetTarget() 
    {
        int nestIndex = Random.Range(0, nestList.Count);
        return nestList[nestIndex];
    }

    public List<T> Shuffle<T>(List<T> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = Random.Range(0, n);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
        return list;
    }
}
