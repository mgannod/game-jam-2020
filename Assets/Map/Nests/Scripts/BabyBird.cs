﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BabyBird : MonoBehaviour
{
    [SerializeField] private GameObject target;
    [SerializeField] private float stepSpeed = 5;

    private Nest nest;

    private float zLayer;

    private bool released = false;

    private void Start()
    {
        zLayer = transform.position.z;
    }

    // Update is called once per frame
    void Update()
    {
        if (released)
        {
            this.transform.position = Vector3.MoveTowards(this.transform.position, target.transform.position, Time.deltaTime * stepSpeed);

            if (Vector3.Distance(this.transform.position, target.transform.position) < .001f)
            {
                Destroy(gameObject);
                nest.SaveBird();
            }
        }
        else
        {

            Vector3 targetPos = new Vector3(target.transform.position.x, target.transform.position.y, zLayer);
            transform.position = targetPos;
        }
    }

    public void SetTarget(Debt target, Nest nest)
    {
        this.nest = nest;
        this.target = target.gameObject;
        target.SetBaby(this);
    }

    public void Release()
    {
        target = nest.gameObject;
        released = true;
    }
}
