﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Nest : MonoBehaviour
{
    [SerializeField] private int birds = 3;
    
    [SerializeField] private BabyBird babyBirdPrefab;

    Animator animator;

    private void Start()
    {
        animator = GetComponent<Animator>();
        animator.SetInteger("birds", birds);
    }

    void Update()
    {
        if (birds <= 0) {SceneManager.LoadScene("TitleScreen", LoadSceneMode.Single); }
    }

    public void AbductBird(Debt enemy)
    {
        birds--;
        updateBirds();

        Vector3 pos = enemy.transform.position;
        pos.z = pos.z - .1f;
        Instantiate(babyBirdPrefab, pos, Quaternion.identity).SetTarget(enemy, this);
        
    }

    public void SaveBird()
    {
        birds++;
        updateBirds();
    }

    private void updateBirds() => animator.SetInteger("birds", birds);
}
